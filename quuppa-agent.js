﻿const dgram = require('dgram');
const server = dgram.createSocket('udp4');
const request = require('request');


const port = 12345;
const eventHubUrl = "http://hmi-event-hub.azurewebsites.net:80/events";

const REQUEST_TIMEOUT = 1500;

// Shake detection
const FORCE_THRESHOLD = 45; // 230 for 50Hz
const TIME_THRESHOLD = 0;
const SHAKE_TIMEOUT = 800; // 400 for 50 Hz
const SHAKE_DURATION = 1000;
const SHAKE_COUNT = 3;

/** Map Tag-ID: Shake detecion data */
const shakeMap = new Map();

/** Mapping Tag-ID: Event */
const lastEvents = new Map();
/** Mapping Tag-ID: Shake */
const isShakedMap = new Map();
/** Mapping Tag-ID: Timer */
const uploadTimer = new Map();
 
server.on('error', (err) => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});

server.on('message', (msg, rinfo) => {

	//console.log(`UDP server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
	//console.log("UDP packet");	
	const event = JSON.parse(msg);
	//if(event.name !== "Asset 2") return; // TODO dont commit
	
	const tagId = event.id;
	let isShaked = isShakedMap.get(tagId);
	if(!isShaked){
		if(detectShake(event))  {
			isShakedMap.set(tagId, true);
		}
	}

	lastEvents.set(tagId, event);

	if(!uploadTimer.get(tagId)){
		const uploadTask = setTimeout(() => {
			const eventToSend = lastEvents.get(tagId);
			let sendNextEventIfNew = (sentEvent) => {
				if (lastEvents.get(tagId) !== sentEvent) {
					sendEvent(lastEvents.get(tagId), tagId, sendNextEventIfNew); // send next event
				}else {
					console.log("Waiting for next event for " + tagId);
					uploadTimer.delete(tagId); // Just wait for next package
				}
			};
			sendEvent(eventToSend, tagId, sendNextEventIfNew);

		}, 0);
		uploadTimer.set(tagId, uploadTask);
	}
});

/**
 * Send a event to backend and notify callback when done
 */
function sendEvent(eventToSend, tagId, callback) {

	console.log(eventToSend.lastPacketTS, "Sending..", );

	let isShaked = isShakedMap.get(tagId);
	if(isShaked) {
		isShakedMap.delete(tagId);
		eventToSend.shock = true;
	}

	request({
		url: eventHubUrl, 
		method: "POST",
		timeout: REQUEST_TIMEOUT,
		json: eventToSend
		}, 
		function (error, response, body) {
			if(error) {
				console.error(eventToSend.lastPacketTS, 'Event Hub request error, message dropped. Reason:', error);
			}else {
				console.log(eventToSend.lastPacketTS, "Event Hub committed", JSON.stringify(eventToSend));
			}
			callback.apply(this, [eventToSend]);
		});
}


/**
 * Detecting shake events, returning true, when shake was detected for a tag 
 */
function detectShake(event) {
	let shakeDetected = false;
    	const tagId = event.id;
    	const now = event.accelerationTS;
	let shakeData = shakeMap.get(tagId);
	if(!shakeData){
		shakeData = {
			lastTime: 0,
			shakeCount: 0,
			lastShake: 0		
		};
		shakeMap.set(tagId, shakeData);
	}

	if ((now - shakeData.lastForce) > SHAKE_TIMEOUT) {
		shakeData.shakeCount = 0;
	}


	if ((now - shakeData.lastTime) > TIME_THRESHOLD) {
		//console.log(now - shakeData.lastTime, " ms", event.acceleration);
		const diff = now - shakeData.lastTime;
		let forceX = event.acceleration ? event.acceleration[0] : 0;
		let forceY = event.acceleration ? event.acceleration[1] : 0;
		let forceZ = event.acceleration ? event.acceleration[2] : 0;
		forceX = forceX ? forceX :0;
		forceY = forceY ? forceY :0;
		forceZ = forceZ ? forceZ :0;

		const speed = Math.abs(Math.abs(forceX - shakeData.lastX) + Math.abs(forceY - shakeData.lastY) + Math.abs(forceZ - shakeData.lastZ)) / diff * 100;
		//console.log(event.name, "Speed: ", Math.round(speed, 2), "Time between: " + (now - shakeData.lastTime) + " ms, Last acc: ", forceX - shakeData.lastX, forceY - shakeData.lastY,
		//forceZ - shakeData.lastZ, event.acceleration, "count :", shakeData.shakeCount);
		if (speed > FORCE_THRESHOLD) {
			if ((++shakeData.shakeCount >= SHAKE_COUNT) && (now - shakeData.lastShake > SHAKE_DURATION)) {
			  shakeData.lastShake = now;
			  shakeData.shakeCount = 0;
			  shakeDetected = true;
			}
			shakeData.lastForce = now;
		}
		shakeData.lastTime = now;
		shakeData.lastX = forceX;
		shakeData.lastY = forceY;
		shakeData.lastZ = forceZ;
	}

	return shakeDetected;
}

server.on('listening', () => {
  const address = server.address();
  console.log(`server listening ${address.address}:${address.port}`);
});

server.bind(port);
// server listening ...
