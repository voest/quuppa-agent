var PORT = 12345;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var client = dgram.createSocket('udp4');

var event = {
    id: 123,
	timestamp: 1234567, 
	position:
	{
		latitude: 53.233434,
		longitude: -14.23234
	},
	zone: "Zone C"
};
var message = JSON.stringify(event);

client.send(message, 0, message.length, PORT, HOST, function(err, bytes) {
    if (err) throw err;
    console.log('UDP message sent to ' + HOST +':'+ PORT);
    client.close();
});
