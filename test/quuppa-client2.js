var PORT = 12345;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var client = dgram.createSocket('udp4');

var event = {
    id: 567,
	name: "Asset 1",
	lastPacketTS: 1234567, 
	smoothedPositionWGS84: [53.233434, -14.23234],
	smoothedPosition: [-1, +0.5, 0],
	zones: [
		{
			id: "0123456789",
			name: "Zone B"
		}
	]
};
var message = JSON.stringify(event);

client.send(message, 0, message.length, PORT, HOST, function(err, bytes) {
    if (err) throw err;
    console.log('UDP message sent to ' + HOST +':'+ PORT);
    client.close();
});
